# Rocket.Chat Telegram Integration

![alt c][logo]


This is just an enhanced version of rocket.chat's [Simple Telegram Bot][0]

## Features

* **Outgoing**
  * Emoji Support: Resolves emojis so telegram users can see them as well
* **Incoming**
  * _**To be done**_
    * Sticker Support: Resolve stickers to their emoji counterpart or find some mapping solution with rocket.chats custom_emoji feature
    * Picture Support: Provide URLs pointint to the posted picture

## Installation

1. Set up the telegram integration using [Simple Telegram Bot][0]
2. When it comes to copy/pasting the integration code, feel free to use the one provided in this repository


## See also

* https://rocket.chat/docs/administrator-guides/integrations/



[0]: https://rocket.chat/docs/administrator-guides/integrations/telegram/#simple-telegram-bot
[logo]: https://gitlab.com/ohsnaparts/public/rocket.chat-telegram-integration/raw/master/img/RocketChatTelegramBridge.png "Rocket.Chat <-> Telegram Bridge"
