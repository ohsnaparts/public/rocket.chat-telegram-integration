/** Global Helpers
 *
 * console - A normal console instance
 * _       - An underscore instance
 * s       - An underscore string instance
 * HTTP    - The Meteor HTTP object to do sync http calls
 *           https://docs.meteor.com/api/http.html
 */


class Script {
    constructor() {
        this.logging = false;
    }

    log(message) {
        if (this.logging) {
            console.log(message);
        }
    }

    /**
     * @returns The resolved emoji character or 'undefined' in case of error or none was found
     */
    resolveEmojiByName(emojiName) {
        this.log(`called: request_emojitext('${emojiName}')`);
        const url = `https://www.emojidex.com/api/v1/emoji/${emojiName}`;

        const response = HTTP('GET', url);

        if (!response || !response.result) {
            console.warn(`Got no response for api call: ${url}`);
            return undefined;
        }

        const statusCode = response.result.statusCode;
        if (statusCode !== 200) {
            console.warn(`Got statusCode other than 200 OK: ${statusCode}`);
            return undefined;
        }

        if (!response.result || !response.result.data || !response.result.data.moji) {
            this.log(`No emoji character found with name: '${emojiName}'`);
            return undefined;
        }

        let emoji = response.result.data.moji;
        this.log(`resolved emoji '${emojiName} to ${emoji}`);

        return emoji;
    }


    /**
     * @summary extracts all emojiCodes from the text
     * @returns all emojiCodes or an empty array if none found
     */
    findEmojiCodes(text) {
        var emojiPattern = /:\w+:/g;

        var matches = text.match(emojiPattern);

        if (!matches || matches.length <= 0) {
            return [];
        }

        //TODO: eliminate duplicates
        return matches;
    }


    /**
     * @summary Tries to resovle and replace all emojis of the text
     * @returns An updated version of text with all emoji codes replaced by emoji characters
     */
    replaceEmojiCodesWithCharacters(text) {
        const emojiCodes = this.findEmojiCodes(text);
        this.log(`Found ${emojiCodes.length} emojis`);

        emojiCodes.forEach(code => {
            const emojiName = code.substring(1, code.length - 1);
            const character = this.resolveEmojiByName(emojiName);
            if (!!character) {
                this.log(`replacing '${code}' with ${character}`);
                text = text.replace(code, character);
            }
        });

        return text;
    }

    /**
    	request.params            {object}
     	request.method            {string}
    	request.url               {string}
    	request.auth              {string}
    	request.headers           {object}
    	request.data.token        {string}
    	request.data.channel_id   {string}
    	request.data.channel_name {string}
    	request.data.timestamp    {date}
    	request.data.user_id      {string}
    	request.data.user_name    {string}
    	request.data.text         {string}
    	request.data.trigger_word {string}
    */
    prepare_outgoing_request({
        request
    }) {
        let text = request.data.text;

        this.log('-------------------------------------------------------------')
        this.log(JSON.stringify(request));
        this.log('');

        if (request.data.bot) {
            //Don't repost messages from the bot.
            return {};
        }

        text = this.replaceEmojiCodesWithCharacters(text);

        this.log(`Sending text: ${text}`);
        this.log('-------------------------------------------------------------')

        return {
            // https://core.telegram.org/bots/api
            // url: request.url + '&parse_mode=HTML' + '&text=' + encodeURIComponent('<b>' + request.data.user_name+ '</b>: ' + request.data.text),

            url: `${request.url}&text=${encodeURIComponent(text)}`,
            method: 'GET'
        };

    }
}